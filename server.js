const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const path = require("path");

var logData = require("./log-data.js");

const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

if (process.env.NODE_ENV === "production") {
  app.use(express.static(path.join(__dirname, "client/build")));
  app.get("*", function(req, res) {
    res.sendFile(path.join(__dirname, "client/build/index.html"));
  });
}

app.listen(port, error => {
  if (error) throw error;
  console.log("Server running on: " + port);
});

app
  .route("/api/eye-log")
  .get(function(req, res) {
    console.log("Requesting log data...");
    res.send(logData);
  })
  .put(function(req, res) {
    const validation = formatAndValidateItem(req.body);
    if (validation.errors) {
      throw new Error(validation.errors.join(", "));
    }
    const item = updateItem(validation.item);
    res.send({ logData, item });
  })
  .post(function(req, res) {
    const validation = formatAndValidateItem(req.body, true);
    if (validation.errors) {
      throw new Error(validation.errors.join(", "));
    }
    const newItem = updateItem(validation.item);
    res.send({ logData, item: newItem });
  })
  .delete(function(req, res) {
    const id = req.body.id;
    if (!id) {
      throw new Error("Could not delete log item. ID not provided.");
    }
    if (logData.find(item => item.id === parseInt(id)) === -1) {
      throw new Error("Could not delete log item. ID not found.");
    }
    if (!deleteItem(parseInt(id))) {
      throw new Error("Could not delete log item. Internal server issue.");
    }
    res.send({ logData });
  });

// Update existing item if exists else Create new item
const updateItem = item => {
  let itemIndex = -1;
  if (item.id) itemIndex = logData.findIndex(logItem => logItem.id === item.id);

  // Updating Existing Item
  if (itemIndex !== -1) {
    logData = [
      ...logData.slice(0, itemIndex),
      item,
      ...logData.slice(itemIndex + 1, logData.length)
    ];
  } else {
    // Creating New Item
    item.id = logData[0].id + 1;
    logData = [item, ...logData];
  }
  return item;
};

const deleteItem = id => {
  const itemIndex = logData.findIndex(logItem => logItem.id === id);
  if (itemIndex !== -1) {
    logData = [
      ...logData.slice(0, itemIndex),
      ...logData.slice(itemIndex + 1, logData.length)
    ];
    return 1;
  }
  return 0;
};

const formatAndValidateItem = (item, isNew = false) => {
  let errors = [];
  const validId = /^[0-9]+$/;
  const validTimestamp = /^[1-9][0-9]{12}$/;
  const validFocalPlane = /^[1-9][0-9]*$/;
  const validCondition = /^(none|strained|rested)$/;
  const validNote = /^[\s\S]{0,600}$/;
  if (!isNew) {
    if (!item.id) {
      errors.push("No Id provided");
    }
    if (!validId.test(String(item.id))) {
      errors.push(
        `Timestamp is not in a valid format. Expected: ${validTimestamp} Received: ${String(
          item.id
        )}`
      );
    }
  }
  if (!item.timestamp) {
    errors.push("No timestamp provided");
  }
  if (!validTimestamp.test(String(item.timestamp))) {
    errors.push(
      `Timestamp is not in a valid format. Expected: ${validTimestamp} Received: ${String(
        item.timestamp
      )}`
    );
  }
  if (!item.leftEye) {
    errors.push("No value for 'leftEye' provided");
  }
  if (!item.rightEye) {
    errors.push("No value for 'rightEye' provided");
  }
  if (!validFocalPlane.test(String(item.leftEye))) {
    errors.push(
      `Left eye value is not in a valid format. Expected: ${validFocalPlane} Received: ${String(
        item.leftEye
      )}`
    );
  }
  if (!validFocalPlane.test(String(item.rightEye))) {
    errors.push(
      `Right eye value is not in a valid format. Expected: ${validFocalPlane} Received: ${String(
        item.rightEye
      )}`
    );
  }
  if (!item.condition) {
    errors.push("No value for 'condition' provided");
  }
  if (!validCondition.test(String(item.condition))) {
    errors.push(
      `Conditaion value is not in a valid format. Expected: ${validCondition} Received: ${String(
        item.condition
      )}`
    );
  }
  if (item.note && !validNote.test(item.note)) {
    errors.push(
      "Note is not in a valid format - note should not be longer than 600 characters"
    );
  }

  if (errors.length > 0) {
    return { errors };
  }
  // Format data
  let formatedItem = {
    id: parseInt(item.id),
    timestamp: parseInt(item.timestamp),
    leftEye: parseInt(item.leftEye),
    rightEye: parseInt(item.rightEye),
    condition: String(item.condition),
    note: String(item.note)
  };
  return { item: formatedItem };
};
