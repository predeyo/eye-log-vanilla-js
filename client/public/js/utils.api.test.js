import {
  getLogData,
  updateLogItem,
  createLogItem,
  deleteLogItem
} from "./utils.api.js";

describe("Utils API", () => {
  describe("GET Log data", () => {
    const mockSuccessResponse = ["data"];
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFailureResponse = null;
    afterEach(() => {
      global.fetch.mockClear();
      delete global.fetch;
    });
    it("should fetch data from server when server returns a successful response", async () => {
      const mockFetchPromise = Promise.resolve({
        ok: true,
        json: () => mockJsonPromise
      });
      global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);
      const data = await getLogData();

      expect(global.fetch).toHaveBeenCalledTimes(1);
      expect(global.fetch).toHaveBeenCalledWith(
        "http://localhost:5000/api/eye-log"
      );
      expect(data).toEqual(mockSuccessResponse);
    });

    it(`should return ${mockFailureResponse} when server return an unsuccessful response`, async () => {
      jest.spyOn(console, "error");
      console.error.mockImplementation(() => {});
      const mockFetchPromise = Promise.resolve({
        ok: false
      });
      global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);

      const data = await getLogData();

      expect(global.fetch).toHaveBeenCalledTimes(1);
      expect(global.fetch).toHaveBeenCalledWith(
        "http://localhost:5000/api/eye-log"
      );
      expect(data).toEqual(mockFailureResponse);
      console.error.mockRestore();
    });
  });

  describe("Update Log item", () => {
    const mockSuccessResponse = { logData: [{ id: 2 }], item: { id: 2 } };
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFailureResponse = null;
    const mockLogItem = { id: 2 };
    const mockFetchArgs = [
      "http://localhost:5000/api/eye-log",
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(mockLogItem)
      }
    ];
    afterEach(() => {
      global.fetch.mockClear();
      delete global.fetch;
    });
    it("should fetch data from server when server returns a successful response", async () => {
      const mockFetchPromise = Promise.resolve({
        ok: true,
        json: () => mockJsonPromise
      });
      global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);

      const data = await updateLogItem(mockLogItem);

      expect(global.fetch).toHaveBeenCalledTimes(1);
      expect(global.fetch).toHaveBeenCalledWith(...mockFetchArgs);
      expect(data).toEqual(mockSuccessResponse);
    });

    it(`should return ${mockFailureResponse} when server return an unsuccessful response`, async () => {
      jest.spyOn(console, "error");
      console.error.mockImplementation(() => {});
      const mockFetchPromise = Promise.resolve({
        ok: false
      });
      global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);

      const data = await updateLogItem(mockLogItem);

      expect(global.fetch).toHaveBeenCalledTimes(1);
      expect(global.fetch).toHaveBeenCalledWith(...mockFetchArgs);
      expect(data).toEqual(mockFailureResponse);
      console.error.mockRestore();
    });
  });

  describe("Create Log item", () => {
    const mockSuccessResponse = { logData: [{ id: 2 }], item: { id: 2 } };
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFailureResponse = null;
    const mockLogItem = { id: 2 };
    const mockFetchArgs = [
      "http://localhost:5000/api/eye-log",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(mockLogItem)
      }
    ];
    afterEach(() => {
      global.fetch.mockClear();
      delete global.fetch;
    });
    it("should fetch data from server when server returns a successful response", async () => {
      const mockFetchPromise = Promise.resolve({
        ok: true,
        json: () => mockJsonPromise
      });
      global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);

      const data = await createLogItem(mockLogItem);

      expect(global.fetch).toHaveBeenCalledTimes(1);
      expect(global.fetch).toHaveBeenCalledWith(...mockFetchArgs);
      expect(data).toEqual(mockSuccessResponse);
    });

    it(`should return ${mockFailureResponse} when server return an unsuccessful response`, async () => {
      jest.spyOn(console, "error");
      console.error.mockImplementation(() => {});
      const mockFetchPromise = Promise.resolve({
        ok: false
      });
      global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);

      const data = await createLogItem(mockLogItem);

      expect(global.fetch).toHaveBeenCalledTimes(1);
      expect(global.fetch).toHaveBeenCalledWith(...mockFetchArgs);
      expect(data).toEqual(mockFailureResponse);
      console.error.mockRestore();
    });
  });

  describe("Delete Log item", () => {
    const mockSuccessResponse = [{ id: 1 }];
    const mockJsonPromise = Promise.resolve(mockSuccessResponse);
    const mockFailureResponse = null;
    const mockLogItem = { id: 2 };
    const mockFetchArgs = [
      "http://localhost:5000/api/eye-log",
      {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(mockLogItem)
      }
    ];
    afterEach(() => {
      global.fetch.mockClear();
      delete global.fetch;
    });
    it("should fetch data from server when server returns a successful response", async () => {
      const mockFetchPromise = Promise.resolve({
        ok: true,
        json: () => mockJsonPromise
      });
      global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);

      const data = await deleteLogItem(mockLogItem.id);

      expect(global.fetch).toHaveBeenCalledTimes(1);
      expect(global.fetch).toHaveBeenCalledWith(...mockFetchArgs);
      expect(data).toEqual(mockSuccessResponse);
    });

    it(`should return ${mockFailureResponse} when server return an unsuccessful response`, async () => {
      jest.spyOn(console, "error");
      console.error.mockImplementation(() => {});
      const mockFetchPromise = Promise.resolve({
        ok: false
      });
      global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);

      const data = await deleteLogItem(mockLogItem.id);

      expect(global.fetch).toHaveBeenCalledTimes(1);
      expect(global.fetch).toHaveBeenCalledWith(...mockFetchArgs);
      expect(data).toEqual(mockFailureResponse);
      console.error.mockRestore();
    });
  });
});
