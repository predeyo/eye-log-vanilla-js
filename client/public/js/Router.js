class Router {
  routes = [];
  root = "/";
  currentRoutes = [];

  constructor(root) {
    if (root) this.root = root;
    this.listen();
  }

  add = (path, callback) => {
    this.routes.push({ path, callback });
  };

  logRoutes = () => {
    console.table(this.routes);
  };

  listen = () => {
    this.checkChanges();
    setInterval(this.checkChanges, 50);
  };

  checkChanges = () => {
    let currentPath = this.getCurrentPath();

    let newCurrentRoutes = [];
    this.routes.forEach(route => {
      let match = currentPath.match(route.path);
      if (!match) return;
      // route callback function arguments are defined by capturing groups,
      // remove first match to leave only capturing groups in match variable
      match.shift();
      let isActiveRoute = this.currentRoutes.find(currentRoute => {
        return (
          currentRoute.path.toString() === route.path.toString() &&
          currentRoute.args.toString() === match.toString()
        );
      });
      if (isActiveRoute) {
        newCurrentRoutes.push({ ...route, args: match });
      } else {
        newCurrentRoutes.push({ ...route, args: match });
        route.callback.apply(null, match);
      }
    });
    this.currentRoutes = newCurrentRoutes;
  };

  getCurrentPath = () => {
    let currentPath = window.location.href.slice(window.location.origin.length);
    // remove search & hash
    currentPath = currentPath.match(/^(?:(?!\?|#).)*/)[0];
    return currentPath === "/" ? currentPath : currentPath.replace(/\/$/, "");
  };
}

export default Router;
