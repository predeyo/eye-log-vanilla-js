import { timestampToDateTime } from "../utils.js";
import eye from "../../assets/right-eye.svg";

export function setLogItemEditTemplate({
  id = null,
  timestamp = Date.now(),
  rightEye = "",
  leftEye = "",
  condition = "none",
  note = ""
}) {
  return `<div class="log-item-edit">
  <form class="log-edit-form" timestamp=${timestamp} id=${id}>
    <nav>
      <button type="submit">
        <i class="fas fa-check" title="Submit"></i>
      </button>
      <button type="button" class="close-modal-action">
        <i class="fas fa-times" title="Close"></i>
      </button>
    </nav>
    <time>${timestampToDateTime(timestamp)}</time>
    <div class="eye-detail-container">
      <div class="eye-details">
        <div class="eye-detail">
          <label>Left</label>
          <img class="eye left-eye" src=${eye} alt="eye"/>
          <span>
            <input
              name="leftEye"
              type="number"
              placeholder="Focal plane distance"
              min="1"
              max="1000000"
              step="1"
              value=${leftEye}
              required
            />
            cm
          </span>
        </div>
        <div class="eye-detail">
          <label>Right</label>
          <img class="eye" src=${eye} alt="eye"/>
          <span>
            <input
              name="rightEye"
              type="number"
              placeholder="Focal plane distance"
              min="1"
              max="1000000"
              step="1"
              value=${rightEye}
              required
            />
            cm
          </span>
        </div>
      </div>
      <div class="condition-container">
        <label>Condition</label>
        <select
          name="condition"
          id="condition"
          value=${condition}
        >
          <option value="strained" ${
            condition === "strained" ? "selected" : ""
          }>Strained</option>
          <option value="none" ${
            condition === "none" ? "selected" : ""
          }>None</option>
          <option value="rested" ${
            condition === "rested" ? "selected" : ""
          }>Rested</option>
        </select>
      </div>
    </div>
    <label>Note</label>
    <textarea
      name="note"
      placeholder="Something to note?"
      cols="30"
      rows="10"
      maxLength="600"
    >${note}</textarea>
  </form>
</div>`;
}
