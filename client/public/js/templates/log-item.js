import { timestampToDateTime } from "../utils.js";

export function setLogItemTemplate({
  timestamp,
  rightEye,
  leftEye,
  condition,
  note,
  id
}) {
  return `<div class="log-item" id="${id}" timestamp="${id}">
            <ul>
              <li class="id">#${id}</li>
              <li class="timestamp">${timestampToDateTime(timestamp)}</li>
              <li class="left-eye">L ${leftEye} cm</li>
              <li class="right-eye">R ${rightEye} cm</li>
              <li class="condition">${condition}</li>
              <li class="note">${note}</li>
            </ul>
          </div>`;
}
