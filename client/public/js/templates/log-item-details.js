import { timestampToDateTime } from "../utils.js";
import eye from "../../assets/right-eye.svg";

export function setLogItemDetailsTemplate({
  timestamp,
  rightEye,
  leftEye,
  condition,
  note,
  id
}) {
  return `<div class="log-item-detail" id=${id}>
    <nav>
      <button class="edit-button">
        <i class="fas fa-pencil-alt" title="Edit"></i>
      </button>
      <button class="delete-button">
        <i class="fas fa-trash-alt" title="Delete"></i>
      </button>
      <button class="close-modal-action">
        <i class="fas fa-times" title="Close"></i>
      </button>
    </nav>
    <time>${timestampToDateTime(timestamp)}</time>
    <div class="eye-detail-container">
      <div class="eye-details">
        <div class="eye-detail">
          <label>Left</label>
          <img class="eye left-eye"  src=${eye} alt="eye"/>
          <span>${leftEye} cm</span>
        </div>
        <div class="eye-detail">
          <label>Right</label>
          <img class="eye"  src=${eye} alt="eye"/>
          <span>${rightEye} cm</span>
        </div>
      </div>
      <div class="condition-container">
        <label>Condition</label>
        <span>${condition}</span>
      </div>
    </div>
    ${
      note
        ? `<div class="note">
        <label>Note</label>
        <p>${note}</p>
      </div>`
        : ""
    }`;
}
