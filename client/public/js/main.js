"use strict";

import "@babel/polyfill";
import Router from "./Router.js";

import {
  getLogData,
  updateLogItem,
  deleteLogItem,
  createLogItem
} from "./utils.api.js";

import { timestampToDateTime } from "./utils.js";
import { setLogItemTemplate } from "./templates/log-item.js";
import { setLogItemDetailsTemplate } from "./templates/log-item-details.js";
import { setLogItemEditTemplate } from "./templates/log-item-edit.js";

function createLogItemDetailsElement(id) {
  const container = document.createElement("div");
  const item = state.logDataMap[id];
  container.innerHTML = setLogItemDetailsTemplate(item);
  container
    .querySelector(".edit-button")
    .addEventListener("click", () =>
      history.pushState(null, "", `/log/${id}/edit`)
    );
  container
    .querySelector(".delete-button")
    .addEventListener("click", () => handleLogItemDeleteClick(id));
  return container;
}

function renderLogItems(items) {
  logListElement.innerHTML = "";
  items.forEach(item => {
    const container = document.createElement("div");
    container.innerHTML = setLogItemTemplate(item);
    container.firstChild.addEventListener("click", handleLogListItemClick);
    logListElement.appendChild(container);
  });
}

function handleLogListItemClick(event) {
  const id = event.currentTarget.id;
  history.pushState(null, "", `/log/${id}`);
}

function renderLogItemDetails(id) {
  renderModal(createLogItemDetailsElement(id), () =>
    history.pushState(null, "", `/`)
  );
}

async function handleLogItemDeleteClick(id) {
  const apiResponse = await deleteLogItem(id);
  if (!apiResponse) {
    alert("Could not delete item - server error, please try again later!");
    return;
  }

  setLogData(apiResponse.logData);
  history.pushState(null, "", `/`);
  handleModalClose();
}

// Edit existing item if id is provided else create new item on submit
function handleLogItemEditClick(id) {
  const container = document.createElement("div");
  const item = id ? state.logDataMap[id] : {};
  container.innerHTML = setLogItemEditTemplate(item);
  container
    .querySelector(".log-edit-form")
    .addEventListener("submit", handleLogItemSumbmitClick);
  if (id) {
    renderModal(
      container,
      () => history.pushState(null, "", `/log/${id}`),
      false
    );
  } else {
    renderModal(container, () => history.pushState(null, "", `/`));
  }
}

async function handleLogItemSumbmitClick(event) {
  event.preventDefault();
  const form = event.currentTarget;
  let formItem = {};
  formItem.id = form.id;
  formItem.timestamp = form.getAttribute("timestamp");
  formItem.leftEye = form.querySelector("[name=leftEye]").value;
  formItem.rightEye = form.querySelector("[name=rightEye]").value;
  formItem.condition = form.querySelector("[name=condition]").value;
  formItem.note = form.querySelector("[name=note]").value;

  let apiResponse;
  // Update existing item
  if (parseInt(formItem.id)) {
    apiResponse = await updateLogItem(formItem);
  }
  // Create new Item
  else {
    apiResponse = await createLogItem(formItem);
  }

  if (!apiResponse) {
    alert("Could not submit item - server error, please try again later!");
    return;
  }

  setLogData(apiResponse.logData);
  history.pushState(null, "", `/log/${apiResponse.item.id}`);
}

function renderModal(content, onClose, closeAnimation = true) {
  // Configure close action from arguments
  let handleClose;
  if (onClose && closeAnimation) {
    handleClose = () => {
      onClose();
      handleModalClose();
    };
  } else if (!onClose && closeAnimation) {
    handleClose = handleModalClose;
  } else if (!onClose && !closeAnimation) {
    handleClose = () => {};
  } else if (onClose && !closeAnimation) {
    handleClose = onClose;
  }
  // Set content
  if (modalContainerElement.classList.contains("closed")) {
    modalElement.innerHTML = "";
    modalElement.appendChild(content);
    // Animate modal fade in
    modalContainerElement.classList.remove("closed");
    setTimeout(() => modalContainerElement.classList.add("fade-in"), 50);
  }
  modalElement.innerHTML = "";
  modalElement.appendChild(content);
  // Set close actions
  modalContainerElement
    .querySelectorAll(".close-modal-action")
    .forEach(element => {
      element.onclick = handleClose;
    });
  document.onkeydown = e => handleEscapeClick(e, handleClose);
}

function handleEscapeClick(e, handleClose) {
  if (e.key === "Escape") handleClose();
}

function handleModalClose() {
  // Close animation & remove content
  modalContainerElement.classList.add("fade-out");
  modalContainerElement.classList.remove("fade-in");
  setTimeout(() => {
    modalElement.innerHTML = "";
    modalContainerElement.classList.add("closed");
    modalContainerElement.classList.remove("fade-out");
  }, 300);
  // Remove event listener
  document.onkeydown = null;
}

function setLogData(logData) {
  state.logData = logData;
  state.logDataMap = {};
  logData.forEach(item => {
    state.logDataMap[item.id] = { ...item };
  });
  renderLogItems(logData);
}

const logListElement = document.getElementById("log-list");
const modalContainerElement = document.querySelector(".modal-container");
const modalElement = document.querySelector(".modal");
const addLogItemButtonElement = document.querySelector(".add-log-item-button");

addLogItemButtonElement.addEventListener("click", () =>
  history.pushState(null, "", `/log/new`)
);

var state = {};
const router = new Router("http://localhost:1234/");

async function init() {
  const data = await getLogData();
  setLogData(data);

  router.add("/log/([1-9][0-9]*)/edit", id => handleLogItemEditClick(id));
  router.add("/log/([1-9][0-9]*)$", id => renderLogItemDetails(id));
  router.add("/log/new$", handleLogItemEditClick);
}

init();
